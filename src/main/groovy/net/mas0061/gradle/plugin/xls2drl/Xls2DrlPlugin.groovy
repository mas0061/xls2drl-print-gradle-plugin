package net.mas0061.gradle.plugin.xls2drl

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * Created by mas on 14/04/02.
 */
class Xls2DrlPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.extensions.create("xls2drlEx", Xls2DrlPluginExtension)
        project.task(type: Xls2DrlPrintTask, "print")
    }
}
