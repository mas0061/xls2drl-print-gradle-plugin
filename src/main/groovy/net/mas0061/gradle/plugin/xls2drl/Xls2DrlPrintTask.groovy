package net.mas0061.gradle.plugin.xls2drl

import org.drools.decisiontable.InputType
import org.drools.decisiontable.SpreadsheetCompiler
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import java.nio.file.Files

import static groovy.io.FileType.FILES

/**
 * Created by mas on 14/04/02.
 */
class Xls2DrlPrintTask extends DefaultTask {
    def static final XLS_MIME_TYPE = "application/vnd.ms-excel"

    @TaskAction
    def print() {
        new File(project.xls2drlEx.resourceDir).traverse(type: FILES, nameFilter: ~/.*\.xls/) { file ->
            println("###### File : " + file.name + " ######");

            SpreadsheetCompiler compiler = new SpreadsheetCompiler();
            println(compiler.compile(Files.newInputStream(file.toPath()), InputType.XLS));
        }
    }
}
