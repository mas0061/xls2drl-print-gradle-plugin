package net.mas0061.gradle.plugin.xls2drl

import net.mas0061.gradle.plugin.xls2drl.Xls2DrlPrintTask
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import org.junit.Test
import static org.junit.Assert.*

/**
 * Created by mas on 14/04/02.
 */
class Xls2DrlPluginTest {
    @Test
    void xls2drlPluginTest() {
        Project project = ProjectBuilder.builder().build()
        project.apply(plugin: "xls2drl")

        assertTrue(project.tasks.print instanceof Xls2DrlPrintTask)
    }
}
